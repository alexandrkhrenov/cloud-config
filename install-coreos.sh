#!/bin/bash
export CONFIG_CLOUD=$1

if ! [ -x "$(command -v ct)" ]; then
 	wget https://github.com/coreos/container-linux-config-transpiler/releases/download/v0.5.0/ct-v0.5.0-x86_64-unknown-linux-gnu
	mv ct-v0.5.0-x86_64-unknown-linux-gnu ct
	chmod +x ct
	mkdir -p /opt/bin
	mv ./ct /opt/bin/ct
	echo "Install ct"
fi

ct -in-file=$CONFIG_CLOUD -out-file ignition.json
coreos-install -d $2 -i ignition.json


